# !/usr/bin/python3
# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'prices.ui'
#
# Created by: PyQt5 UI code generator 5.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(718, 387)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(270, 0, 191, 51))
        font = QtGui.QFont()
        font.setPointSize(20)
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.lbl_dow = QtWidgets.QLabel(self.centralwidget)
        self.lbl_dow.setGeometry(QtCore.QRect(20, 50, 111, 31))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.lbl_dow.setFont(font)
        self.lbl_dow.setObjectName("lbl_dow")
        self.lbl_btc = QtWidgets.QLabel(self.centralwidget)
        self.lbl_btc.setGeometry(QtCore.QRect(20, 140, 91, 31))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.lbl_btc.setFont(font)
        self.lbl_btc.setObjectName("lbl_btc")
        self.lbl_dow_price = QtWidgets.QLabel(self.centralwidget)
        self.lbl_dow_price.setGeometry(QtCore.QRect(150, 50, 111, 31))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.lbl_dow_price.setFont(font)
        self.lbl_dow_price.setText("")
        self.lbl_dow_price.setObjectName("lbl_dow_price")
        self.lbl_dow_change = QtWidgets.QLabel(self.centralwidget)
        self.lbl_dow_change.setGeometry(QtCore.QRect(150, 80, 111, 31))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.lbl_dow_change.setFont(font)
        self.lbl_dow_change.setText("")
        self.lbl_dow_change.setObjectName("lbl_dow_change")
        self.lbl_btc_price = QtWidgets.QLabel(self.centralwidget)
        self.lbl_btc_price.setGeometry(QtCore.QRect(150, 140, 91, 31))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.lbl_btc_price.setFont(font)
        self.lbl_btc_price.setText("")
        self.lbl_btc_price.setObjectName("lbl_btc_price")
        self.lbl_btc_change = QtWidgets.QLabel(self.centralwidget)
        self.lbl_btc_change.setGeometry(QtCore.QRect(150, 170, 91, 31))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.lbl_btc_change.setFont(font)
        self.lbl_btc_change.setText("")
        self.lbl_btc_change.setObjectName("lbl_btc_change")
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "Current Prices"))
        self.lbl_dow.setText(_translate("MainWindow", "DOW Market"))
        self.lbl_btc.setText(_translate("MainWindow", "BTC Price"))


#!/usr/bin/python3

import requests
from PyQt5 import QtWidgets
from urllib.request import urlopen as uReq
from bs4 import BeautifulSoup
from bs4 import BeautifulSoup as soup
from prices import Ui_MainWindow  # importing our generated file

import sys


class mywindow(QtWidgets.QMainWindow):

    def __init__(self):
        super(mywindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.lbl_dow_price.setText(closed_price)
        self.ui.lbl_dow_change.setText(change)
        self.ui.lbl_btc_price.setText(btc_price)


################################################
# DOW  Price to Fetch
################################################
dow_url = "https://quotes.wsj.com/index/DJIA/historical-prices"
# open, read and close connection to website
dow_connection = uReq(dow_url)
dow_page_html = dow_connection.read()
dow_connection.close()
# we want html parsing
page_soup = soup(dow_page_html, "html.parser")
# search from the parsing to find the information
page_quote = page_soup.findAll("span", {"class":"cr_num"})
# narrow down the information to get the actual information
for quote in page_quote:
    price = page_soup.findAll("span", {"id":"quote_val"})
# clean up the information to present it
    closed_price = price[0].text.strip()
# pull some more information and clean it up
    find_change = page_soup.findAll("span", {"id":"quote_change"})
    change = find_change[0].text.strip()

################################################
# BTC Current Price Fetch
################################################
#API URLS Start
BITCOIN_API_URL = 'https://api.coinmarketcap.com/v1/ticker/bitcoin/'
BAT_API_URL = 'https://api.coinmarketcap.com/v1/ticker/basic-attention-token/'
ETH_API_URL = 'https://api.coinmarketcap.com/v1/ticker/ethereum/'
#API URLS End
#API Requests
bitcoin_response = requests.get(BITCOIN_API_URL)
for btc_coin in bitcoin_response.json():
    btc_price = ("BTC Price $" + btc_coin["price_usd"])

app = QtWidgets.QApplication([])

application = mywindow()

application.show()

sys.exit()

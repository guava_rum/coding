#!/usr/bin/python3
# webscraper for financial information

import requests
import json
from urllib.request import urlopen as uReq
from bs4 import BeautifulSoup
from bs4 import BeautifulSoup as soup
print("\n")
print("****************")
print("* Stock Market *")
print("****************")
print("\n")
################################################
# DOW  Price to Fetch
################################################
dow_url = "https://quotes.wsj.com/index/DJIA/historical-prices"
# open, read and close connection to website
dow_connection = uReq(dow_url)
dow_page_html = dow_connection.read()
dow_connection.close()
# we want html parsing
page_soup = soup(dow_page_html, "html.parser")
# search from the parsing to find the information
page_quote = page_soup.findAll("span", {"class":"cr_num"})
# narrow down the information to get the actual information
for quote in page_quote:
    price = page_soup.findAll("span", {"id":"quote_val"})
# clean up the information to present it
    closed_price = price[0].text.strip()
# pull some more information and clean it up
    find_change = page_soup.findAll("span", {"id":"quote_change"})
    change = find_change[0].text.strip()
print("Close: $" + closed_price)
print("Change: " + change)

print("\n")
print("*************************")
print("* CryptoCurrency Prices *")
print("*************************")
print("\n")
################################################
# BTC Current Price Fetch via requests method
################################################
#API URLS Start
BITCOIN_API_URL = 'https://api.coinmarketcap.com/v1/ticker/bitcoin/'
BAT_API_URL = 'https://api.coinmarketcap.com/v1/ticker/basic-attention-token/'
ETH_API_URL = 'https://api.coinmarketcap.com/v1/ticker/ethereum/'
#API URLS End
#API Requests
bitcoin_response = requests.get(BITCOIN_API_URL)
for btc_coin in bitcoin_response.json():
    print("BTC Price $" + btc_coin["price_usd"])
for btc_coin_change in bitcoin_response.json():
    print("BTC Change % " + btc_coin_change["percent_change_24h"])
print("---------------")
bat_response = requests.get(BAT_API_URL)
for bat_coin in bat_response.json():
    print("BAT Price $" + bat_coin["price_usd"])
for bat_coin_change in bat_response.json():
    print("BAT Change % " + bat_coin_change["percent_change_24h"])
print("---------------")
eth_response = requests.get(ETH_API_URL)
for eth_coin in eth_response.json():
    print("ETH Price $" + eth_coin["price_usd"])
for eth_coin_change in eth_response.json():
    print("ETH Change % " + eth_coin_change["percent_change_24h"])
print("---------------")

print("\n")
print("*************************")

#!/usr/bin/python3
# Convert Fahrenheit to Celsius and Vice Versa GUI


from PyQt5 import QtWidgets, QtGui

from f2c import Ui_MainWindow  # importing our generated file

import sys

class mywindow(QtWidgets.QMainWindow):

    def __init__(self):
        super(mywindow, self).__init__()
        self.setWindowIcon(QtGui.QIcon('icon.png'))
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.print_celsius.clicked.connect(self.fahrenheit)
        self.ui.print_fahrenheit.clicked.connect(self.celsius)
        self.title = "Temperature Converter"
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.show()

    def fahrenheit(self):
            try:
                temp_input = int(self.ui.fah_number_box.toPlainText())
                temp_fconvert = (temp_input - 32) * 5 / 9
                f2c = round(temp_fconvert)
                self.ui.f2c_box.setPlainText(str(f2c))
            except (ValueError, IOError):
                self.ui.f2c_box.setPlainText(str("Not Possible"))



    def celsius(self):
        try:
            ctemp_input = int(self.ui.cel_number_box.toPlainText())
            temp_cconvert = (ctemp_input * 9 / 5) + 32
            c2f = round(temp_cconvert)
            self.ui.c2f_box.setPlainText(str(c2f))
        except (ValueError, IOError):
            self.ui.c2f_box.setPlainText(str("Not Possible"))


app = QtWidgets.QApplication([])


application = mywindow()

application.show()


sys.exit(app.exec_())
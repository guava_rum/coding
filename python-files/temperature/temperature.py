#! /usr/bin/python3
# Convert Fahrenheit to Celsius and vice versa
#

import sys

print("\n")
print("*****Convert Fahrenheit to Celsius and vice versa*****")
print("\n")
cf = True
while cf:
    cf = str(input("Fahrenheit(f) or Celsius(c) or Quit(q)? "))
    # Convert fahrenheit
    if cf == "f":
        def f_to_c(f_temp):
            return (f_temp - 32) * 5 / 9
        fah_number = int(input('Enter Fahrenheit: '))
        f_in_celsius = f_to_c(fah_number)
        f2c = round(f_in_celsius)
        print(str(fah_number) + " Fahrenheit is equal to " + str(f2c) + " Celsius")
    # Convert celsius
    elif cf == "c":
        def c_to_f(c_temp):
            return (c_temp * 9 / 5) + 32
        c= int(input('Enter Celsius: '))
        c_in_fahrenheit = c_to_f(cel_number)
        c2f = round(c_in_fahrenheit)
        print(str(cel_number) + " Celsius is equal to " + str(c2f) + " Fahrenheit")
    elif cf == "q":
        print ("Goodbye")
        sys.exit()
    elif cf != "":
        print ("Unknown Option")

